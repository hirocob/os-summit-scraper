os-summit-scraper
=================

A scraper for collecting OpenStack Summit schedule.

Installation
------------

* Put the follwing command at the top directory to install oscraper:

  ``$ python setup.py install``

* If you want to insall in develop mode, then put:

  ``$ python setup.py develop``

How to use?
-----------

* Get all events information in json

  ``$ oscraper --summit <summit name> > schedule.json``

* Import the json file into MongoDB

  ``$ mongoimport --host=<host ip> --port=27017 -d <database name> -c <collection name> --jsonArray -vvvv schedule.json``
