#  Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import time

from selenium import webdriver

BROWSER_PATH = '/usr/local/Caskroom/chromedriver/2.38/chromedriver'

logger = logging.getLogger(__name__)


class JSExecutor(object):

    def __init__(self):
        self.driver = webdriver.Chrome()

    def get_completed_page(self, url):
        """Get and execute specified url and return full HTML page text."""

        try:
            self.driver.get(url)
            time.sleep(3)
        except Exception as e:
            logger.ERROR('ERROR: ' + e)
            return None

        return self.driver.page_source
