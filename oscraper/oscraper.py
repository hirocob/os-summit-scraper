# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import json
import logging

from bs4 import BeautifulSoup

from oscraper.utils import js_executor

BASE_URL = "https://www.openstack.org/summit/%s/summit-schedule/full/"
SUMMIT_NAME = "vancouver-2018"

logger = logging.getLogger(__name__)


def get_events(summit_name=None):
    """Get all events from the schedule page.

    @return: a list of event dictionary
    """

    summit = summit_name if summit_name else SUMMIT_NAME

    jsx = js_executor.JSExecutor()
    url = BASE_URL % summit
    page = jsx.get_completed_page(url)

    if page is None:
        logging.ERROR('ERROR: Failed to get ' + url)
        return None

    bs = BeautifulSoup(page, "lxml")
    sections = bs.select('div.panel.panel-default')

    events = []
    for s in sections:
        day = s.select_one('div.panel-heading').text
        ths = [th.text for th in s.select_one('thead').select('th')]

        for row in s.select_one('tbody').select('tr'):
            tds = row.select('td')
            event = {}
            event['id'] = row.attrs['data-id']
            for i in range(len(ths)):
                if ths[i] == 'Time':
                    event[ths[i]] = day + ' ' + tds[0].text
                else:
                    event[ths[i]] = tds[i].text

            events.append(event)

    return events


def main():
    """Entry point to run oscraper."""

    parser = argparse.ArgumentParser(description='Get summit information')
    parser.add_argument('--summit', action='store', dest='summit_name',
                        help='Specify a summit name (e.g. vancouver-2018)')

    params = parser.parse_args()
    summit = params.summit_name if params.summit_name else SUMMIT_NAME

    events = get_events(summit)

    if events:
        print(json.dumps(events))
    else:
        print('No event was found.')


if __name__ == '__main__':
    main()
